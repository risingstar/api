const tap = require('tap')

class SuperProxy {
  constructor(fields) {
    this.fields = fields
    return new Proxy({}, this)
  }

  get (obj, prop) {
    if (this.fields.includes(prop)) {
      return obj[prop]
    }
    throw new ReferenceError()
  }
}

const p = new SuperProxy(['a', 'b'])

p.a = 1
p.b = undefined

tap.same(p.a, 1)
tap.same(p.b, undefined)
tap.notOk('c' in p)
tap.throws(() => p.c)

class Organization extends SuperProxy {
  constructor() {
    super(['identifier', 'legalName'])
  }
}

const eh = new Organization()

eh.identifier = 3
tap.same(eh.identifier, 3)
tap.throws(() => eh.status)
