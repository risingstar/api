const tap = require('tap')
tap.comment('check pagination for Organizations')

const { gql } = require('apollo-server')
const ApiServer = require('../app/ApiServer')
const ApiClient = require('../app/ApiClient')

tap.test('query for multiple organisations, returning cursor', async t => {
  const api = new ApiServer()
  const { url, server } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  const client = new ApiClient(url)

  let data = await client.query({
    query: gql`
      {
        OrganizationFeed(orgStatus: "Active", size: 20) {
          organizations {
            legalName
            identifier
            registeredAddress {
              postalCode
              postCodeUnit {
                ru11Ind
              }
            }
          }
          cursor
        }
      }
    `
  })

  /**
   * Check that we have 20 results and a cursor:
   */
  const feed1 = data.data.OrganizationFeed
  t.same(feed1.organizations.length, 20)
  t.ok(feed1.cursor)

  /**
   * Query for the next 10 results, using the cursor from the previous
   * search:
   */
  data = await client.query({
    query: gql`
      query ($cursor: String) {
        OrganizationFeed(cursor: $cursor) {
          organizations {
            legalName
            identifier
          }
          cursor
        }
      }
    `,
    variables: {
      cursor: feed1.cursor
    }
  })

  /**
   * Check that we have 20 more results and a cursor:
   */
  const feed2 = data.data.OrganizationFeed
  t.same(feed2.organizations.length, 20)
  t.ok(feed2.cursor)

  /**
   * Now check that the results in the two queries are different:
   *
   * NOTE: We can't compare the cursors because the ES docs say that sometimes
   * the cursor will change between requests and sometimes not.
   */

  const compare = (a, b) => {
    if (a.identifier < b.identifier) {
      return -1
    }
    if (a.identifier > b.identifier) {
      return 1
    }
    return 0
  }
  feed1.organizations.sort(compare)
  feed2.organizations.sort(compare)
  t.notSame(feed1.organizations, feed2.organizations)

  server.close()
  t.end()
})

/**
 * TODO(MB): Everything here looks correct, based on reading the docs. But
 * it doesn't actually work. Although the data *inside* the updateQuery()
 * method is correct (there are 20 items after merging the first 10 with
 * the second 10), the data *outside* the query is incorrect--only the
 * newest values are present, i.e., the second 10.
 */
tap.skip('query for multiple organisations, using fetchMore', async t => {
  const api = new ApiServer()
  const { url, server } = await api.listen()
  t.same(url, 'http://localhost:4000/')

  const client = new ApiClient(url)

  /**
   * Create a handle for a watch query. Note that it won't actually run until
   * there is a call to subscribe():
   */
  const observable = client.watchQuery({
    query: gql`
      query ($orgStatus: String, $cursor: String) {
        OrganizationFeed(orgStatus: $orgStatus, cursor: $cursor) @connection(key: "OrganizationFeed") {
          organizations {
            legalName
            identifier
          }
          cursor
        }
      }
    `,
    variables: {
      orgStatus: 'Active'
    }
  })
  await observable.subscribe()

  let data = await observable.result()

  /**
   * Check that we have 10 results and a cursor:
   *
   * NOTE: The reason there are 10 results is because that is the default for
   * Elasticsearch. If the source of Organizations were to change then this
   * default might change, so ideally the caller would have more control over
   * this value.
   */
  const feed1 = data.data.OrganizationFeed
  t.same(feed1.organizations.length, 10)
  t.ok(feed1.cursor)

  /**
   * Query for the next 10 results, using the cursor from the previous
   * search. The results of the new search will be merged with the previous
   * one:
   */
  data = await observable.fetchMore({
    variables: {
      cursor: feed1.cursor
    },
    updateQuery: (prev, { fetchMoreResult }) => {
      /**
       * Check that we've received the previous results and some new results:
       */
      t.ok(prev)
      t.ok(fetchMoreResult)

      /**
       * Also check that both lists have 10 records:
       */
      t.same(prev.OrganizationFeed.organizations.length, 10)
      t.same(fetchMoreResult.OrganizationFeed.organizations.length, 10)

      /**
       * Merge the new results into the old:
       */
      const results = {
        ...fetchMoreResult,
        OrganizationFeed: {
          ...fetchMoreResult.OrganizationFeed,
          organizations: [
            ...prev.OrganizationFeed.organizations,
            ...fetchMoreResult.OrganizationFeed.organizations
          ]
        }
      }

      /**
       * Verify that there are now 20 results:
       */
      t.same(results.OrganizationFeed.organizations.length, 20)

      return results
    }
  })

  /**
   * Check that the list  now has 20 results and a cursor:
   */
  const feed2 = data.data.OrganizationFeed
  t.same(feed2.organizations.length, 20)
  t.ok(feed2.cursor)

  server.close()
  t.end()
})
