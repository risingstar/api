const tf = require('@tensorflow/tfjs')
require('@tensorflow/tfjs-node')

const orgCategoryMap = require('./orgCategory')
const orgRurIndMap = require('./orgRurInd')
const orgStatusMap = require('./orgStatus')
const orgTypeMap = require('./orgType')

const {
  HYPER_LEARNING_RATE,
  HYPER_LOSS,
  MODEL_FILE
} = process.env

class PredictionDataSource {
  get query() {
    return {
      Prediction: async args => {
        const modelFile = MODEL_FILE
        /**
         * Normalise the parameters:
         */
        const category = orgCategoryMap.scale0To1(
          orgCategoryMap.normalise(args.orgCategory)
        )
        const rural = orgRurIndMap.scale0To1(
          orgRurIndMap.normalise(args.rural)
        )
        const type = orgTypeMap.scale0To1(
          orgTypeMap.normalise(args.orgType)
        )
        /**
         * Load the model:
         */
        const model = await tf.loadModel(modelFile)
        const optimizer = tf.train.adam(+HYPER_LEARNING_RATE)
        model.compile({
          optimizer,
          loss: HYPER_LOSS,
          metrics: ['accuracy']
        })
        const predictXs = tf.tensor2d([[category, type, rural]])
        const prediction = await model.predict(predictXs).data()
        return {
          orgStatus: 'Active',
          confidence: Math.floor(prediction[0] * 100)
        }
      }
    }
  }

  get type() {
    return {
      Prediction: class Prediction {
        constructor(prediction) {
          this.orgStatus = prediction.orgStatus
          this.confidence = prediction.confidence
        }
      }
    }
  }
}

module.exports = PredictionDataSource
