/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html } from '@polymer/lit-element';
import { connect } from 'pwa-helpers/connect-mixin.js';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

// These are the elements needed by this element.
import { predictions } from './predictions.js';
import './org-category.js';
import './org-type.js';
import './org-rurind.js';

class MyViewPredict extends PageViewElement {
  _runQuery() {
    console.log('runQuery', this.category, this.rurInd, this.type)
    let prediction = 'combination not found'
    for (const p of predictions) {
      if (p[0] === this.category && p[1] === this.type && p[2] === this.rurInd) {
        console.log('found:', p[3]);
        switch (p[3]) {
          case 73:
            prediction = 'Low risk'
            break;
          case 72:
            prediction = 'Low to average risk'
            break;
          case 71:
          case 70:
            prediction = 'Average risk'
            break;
          case 69:
            prediction = 'Average to high risk'
            break;
          case 68:
            prediction = 'High risk'
            break;
        }
        this.prediction = prediction
        break;
      }
    }
  }

  _setCategory(e) {
    this.category = e.detail.value
    this._runQuery()
  }

  _setRurInd(e) {
    this.rurInd = e.detail.value
    this._runQuery()
  }

  _setType(e) {
    this.type = e.detail.value
    this._runQuery()
  }

  static get properties() { return {
    /* The prediction. */
    prediction: { type: String },
  }};

  render() {
    return html`
      ${SharedStyles}
      <section>
        <h2>Predict</h2>
        <p>
          On this page you can enter some information about a company and get a risk value.
        </p>
      </section>

      <section>

        <org-rurind @org-rurind-changed="${this._setRurInd}"></org-rurind>
        <org-type @org-type-changed="${this._setType}"></org-type>
        <org-category @org-category-changed="${this._setCategory}"></org-category>

        Prediction: <span>${this.prediction}</span>
      </section>
    `
  }
}

window.customElements.define('my-view-predict', MyViewPredict);
