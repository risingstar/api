/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { LitElement, html } from '@polymer/lit-element';

import '@polymer/paper-dropdown-menu/paper-dropdown-menu.js';
import '@polymer/paper-item/paper-item.js';
import '@polymer/paper-listbox/paper-listbox.js';

// This element is *not* connected to the Redux store.
class OrgRurInd extends LitElement {
  constructor() {
    super()
    this.lookup = new Map([
      ['Megapolis', 0],
      ['Rural', 1]
    ])
  }

  _onValueChanged(e) {
    console.log('orgRurInd', this.lookup.get(e.detail.value))
    this.dispatchEvent(new CustomEvent(
      'org-rurind-changed',
      {
        detail: {
          value: this.lookup.get(e.detail.value)
        }
      }
    ));
  }

  render() {
    return html`
      <style>
        :host { display: block; }
      </style>
      <paper-dropdown-menu label="orgRurInd" @value-changed="${this._onValueChanged}">
        <paper-listbox slot="dropdown-content" selected="0">
          <paper-item value="0">Megapolis</paper-item>
          <paper-item value="1">Rural</paper-item>
        </paper-listbox>
      </paper-dropdown-menu>
    `;
  }
}

window.customElements.define('org-rurind', OrgRurInd);
